package com.example.chatandroidclient;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatandroidclient.HandlerControl.HandlerListener;

public class MainActivity extends Activity implements HandlerListener{
    private EditText msgEditText = null;
    private Button sendMsgButton = null;
    private Button sendBmpButton = null;
    private Button sendVocButton = null;
    private TextView msgTextView = null;
    private ImageView image = null;
    private SocketClient socketClient = null;
    private ScrollView scrollView = null;
    private Handler mHandler = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msgEditText = (EditText)findViewById(R.id.ip_msg);
        sendMsgButton = (Button)findViewById(R.id.send_msg_btn);
        sendBmpButton = (Button)findViewById(R.id.send_bmp_btn);
        sendVocButton = (Button)findViewById(R.id.send_voi_btn);
        msgTextView = (TextView)findViewById(R.id.msg_area);
        scrollView = (ScrollView)findViewById(R.id.scroll);
        image = (ImageView)findViewById(R.id.image);
        mHandler = new Handler();
        HandlerControl.getInstance().addHandlerListener(this);
        //初始化socket链接
        if(socketClient == null){
            final String ip = "192.168.1.12";
            new Thread(){

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    super.run();
                    socketClient = new SocketClient(ip);
                }

            }.start();

        }
        setListeners();

    }

    private void setListeners() {
        sendBmpButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(socketClient != null){
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                    socketClient.sendBitmap(bitmap);
                }else{
                    Toast.makeText(MainActivity.this, "socket == null", 1000).show();
                }

            }
        });
        sendVocButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

            }
        });
        sendMsgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(socketClient != null){
                    String msg =  msgEditText.getText().toString();
                    socketClient.sendMsg(msg);
                }else{
                    Toast.makeText(MainActivity.this, "socket == null", 1000).show();
                }
            }
        });
        msgEditText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //这里必须要给一个延迟，如果不加延迟则没有效果。我现在还没想明白为什么
                mHandler.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        //将ScrollView滚动到底
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                }, 100);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public static Bitmap bitmap = null;
    @Override
    public void onTrigger(TriggerInfo triggerInfo) {
        // TODO Auto-generated method stub
        StringBuilder stringBuilder;
        switch (triggerInfo.m_iTriggerID) {
            case TriggerID.MESSAGE_CHAT:
                stringBuilder = new StringBuilder( msgTextView.getText().toString());
                stringBuilder.append(triggerInfo.m_String1);
                msgTextView.setText(stringBuilder);
                break;
            case TriggerID.MESSAGE_IMAGE:
                stringBuilder = new StringBuilder( msgTextView.getText().toString());
                stringBuilder.append(triggerInfo.m_String1);
                msgTextView.setText(stringBuilder);
                if(bitmap != null){
                    image.setImageBitmap(bitmap);
                }
                break;
            default:
                break;
        }
    }

}
