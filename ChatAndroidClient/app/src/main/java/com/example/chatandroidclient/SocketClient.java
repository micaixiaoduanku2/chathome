package com.example.chatandroidclient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
/*
 * 协议内容：
 * 's':连接请求
 * 'm':消息
 * 'b':图片
 * 'v':声音
 */
public class SocketClient extends Thread{
    Socket socket;
    DataInputStream in;
    DataOutputStream out;
    boolean flag = false;
    String name,chat_txt,chat_in;
    public static final int PORT = 8521;
    public SocketClient(String ip){
        name = "小立";
        try {
            socket = new Socket(ip,PORT);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            Date now = new Date(System.currentTimeMillis());
            SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
            String nowStr = format.format(now);
            char p = 's';
            out.write((byte)p);
            out.writeUTF("$$"+name+""+nowStr+" 上线了!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        start();
    }
    @Override
    public void run() {
        // TODO Auto-generated method stub
        super.run();
        //协议头
        char p;
        while(true){
            try {
                //读取协议头
                p = (char) in.readByte();
                switch(p){
                    case 's':
                    case 'm':
                        chat_in = in.readUTF();
                        TriggerInfo trimsg = new TriggerInfo(TriggerID.MESSAGE_CHAT);
                        trimsg.SetString1("收到服务器消息:"+chat_in+"\n");
                        HandlerControl.getInstance().sendTrigger(trimsg);
                        break;
                    case 'b':
                        Log.i("data", "receive 协议头 b");
                        int length = in.readInt();
                        Log.i("data", "receive length "+length);
                        byte[] data = new byte[length];
                        int len = 0;
                        while(len < length){
                            len += in.read(data,len,length-len);
                        }
                        MainActivity.bitmap = BitmapFactory.decodeByteArray(data, 0, length);
                        TriggerInfo tri = new TriggerInfo(TriggerID.MESSAGE_IMAGE);
                        tri.SetString1("收到一张图片");
                        HandlerControl.getInstance().sendTrigger(tri);
                        break;
                    case 'v':
                        break;
                    default:
                        break;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void sendMsg(String msg){
        try {
            char p = 'm';
            out.write((byte)p);
            out.writeUTF(name+":"+msg);
            out.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public void sendBitmap(Bitmap bitmap){
        byte[] data = Bitmap2Bytes(bitmap);
        try {
            char p = 'b';
            out.write((byte)p);
            int length = data.length;
            Log.i("data", "data send length "+length);
            out.writeInt(length);
            out.write(data);
            out.flush();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // 获得本机ＣＰＵ大小端
    public static boolean isBigendian() {
        short i = 0x1;
        boolean bRet = ((i >> 8) == 0x1);
        return bRet;
    }
}
