import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class Sever extends JFrame implements ActionListener{
	//建立服务器端主界面中所用到的布局方式
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	
	JPanel jPanel1 = new JPanel();
	JPanel jPanel2 = new JPanel();
	JButton jButton1 = new JButton();
	JButton jButton2 = new JButton();
	JScrollPane jScrollPane1 = new JScrollPane();
	
	//创建服务器接受信息文本框
	static JTextArea jTextArea1 = new JTextArea();
	boolean bool = false,start = false;
	//声明serverThread线程类对象
	ServerThread serverThread;
	Thread thread;
	//构造函数，用于初始化
	public Sever(){
		super("Server");
		//设置内容面板布局方式
		getContentPane().setLayout(borderLayout1);
		//初始化按钮组件
		jButton1.setText("启动服务器");
		jButton1.addActionListener(this);
		jButton2.setText("关闭服务器");
		jButton2.addActionListener(this);
		//初始化jPanel1面板对象，并向其加入组件
		this.getContentPane().add(jPanel1, java.awt.BorderLayout.NORTH);
		jPanel1.add(jButton1);
		jPanel1.add(jButton2);
		
		//初始化jPanel2面板对象，并向其加入组件
		jTextArea1.setText("");
		jPanel2.setLayout(borderLayout2);
		jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);
		jScrollPane1.getViewport().add(jTextArea1);
		this.getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);
		
		this.setSize(400, 400);
		this.setVisible(true);
		
	}
	
	public static void main(String[] args){
		Sever sever = new Sever();
		sever.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == jButton1){
			serverThread = new ServerThread();
			serverThread.start();
		}else if(e.getSource() == jButton2){
			bool = false;
			start = false;
			serverThread.finalize();
			this.setVisible(false);
		}
	}
	

}
