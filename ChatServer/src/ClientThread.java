import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/*
 * 协议内容：
 * 's':连接请求
 * 'm':消息
 * 'b':图片
 * 'v':声音
 */
public class ClientThread extends Thread{
	//维持服务器与单个客户端的连接线程，负责接收客户端发来的信息
	Socket clientSocket;
	
	DataInputStream in = null;
	DataOutputStream out = null;
	
	ServerThread serverThread;
	String str;
	
	public static int ConnectNumber=0;
	public ClientThread(Socket socket, ServerThread serverThread){
		clientSocket = socket;
		this.serverThread = serverThread;
		try{
			//创建服务器端数据输入输出流
			in = new DataInputStream(clientSocket.getInputStream());
			out = new DataOutputStream(clientSocket.getOutputStream());
		}catch(IOException e2){
			System.out.println("发生异常"+e2);
			System.out.println("建立I/O通道失败!");
			System.exit(3);
		}
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			String message;
			try {
				byte p = in.readByte();
				System.out.println("收到协议头 "+(char)p);
				switch((char)p){
					case 's':
					case 'm':
						message = in.readUTF();
						if(message !=null){
							serverThread.messages.addElement(message);
							Sever.jTextArea1.append(message+'\n');
						}
						break;
					case 'b':
						System.out.println("receive 协议头 b");
						int length = in.readInt();
						System.out.println("receive length "+length);
						byte[] data = new byte[length];
						int len = 0;    
	                    while (len < length) {    
	                        len += in.read(data, len, length - len);    
	                    }    
						serverThread.bitmaps.addElement(data);
						break;
					case 'v':
						break;
						default:
							break;
				}
//				System.out.println("大小端 ： "+isBigendian());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	// 获得本机ＣＰＵ大小端    
    public static boolean isBigendian() {    
        short i = 0x1;    
        boolean bRet = ((i >> 8) == 0x1);        
        return bRet;    
    }    
	
}
