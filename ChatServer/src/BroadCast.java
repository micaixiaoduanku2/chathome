import java.io.IOException;


public class BroadCast extends Thread{
	ClientThread clientThread;
	ServerThread serverThread;
	//聊天消息
	String str;
	//聊天图片数据
	byte[] data;
	
	public BroadCast(ServerThread serverThread){
		this.serverThread = serverThread;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			str = null;
			data = null;
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			synchronized (serverThread.messages) {
				if(!serverThread.messages.isEmpty()){
					str = (String)this.serverThread.messages.firstElement();
					System.out.println("收到字符串   "+str);
				}
			}
			synchronized (serverThread.bitmaps) {
				if(!serverThread.bitmaps.isEmpty()){
					data = this.serverThread.bitmaps.firstElement();
					System.out.println("收到图片数据   ");
				}
			}
			synchronized (serverThread.clients) {
				if(str != null || data != null){
					for(int i =0 ;i < serverThread.clients.size(); i++){
						clientThread = (ClientThread)serverThread.clients.elementAt(i);
						try {
							if(str != null){
								System.out.println("发送消息协议 m");
								clientThread.out.write((byte)'m');
								System.out.println("发送消息   "+str);
								clientThread.out.writeUTF(str);
								clientThread.out.flush();
								this.serverThread.messages.removeElement(str);
							}
							if(data != null){
								System.out.println("发送图片协议 b");
								clientThread.out.write((byte)'b');
								int length = data.length;
								System.out.println("发送图片协议 长度"+data.length);
								clientThread.out.writeInt(length);
								System.out.println("发送图片数据");
								clientThread.out.write(data);
								clientThread.out.flush();
								this.serverThread.bitmaps.removeElement(data);
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				}	
			}
		}
	}
	
	
}

