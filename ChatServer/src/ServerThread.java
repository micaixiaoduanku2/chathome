import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Vector;


public class ServerThread extends Thread{
	ServerSocket serverSocket;
	public static final int PORT = 8521;
	//创建一个vector对象，用于存储客户端连接的ClientThread对象
	//ClientThread类维持服务器与单个客户端的连接线程
	//负责接收客户端发来的信息
	Vector<ClientThread> clients;
	
	Vector<Object> messages;
	
	Vector<byte[]> bitmaps;
	
	//声明BroadCast类对象
	//BroadCast类负责服务器向客户端广播消息
	BroadCast broadcast;
	
	String ip;
	InetAddress myIPaddress = null;
	
	
	public static String getRealIp() throws SocketException {
        String localip = null;// 本地IP，如果没有配置外网IP则返回它
        String netip = null;// 外网IP
 
        Enumeration<NetworkInterface> netInterfaces = 
            NetworkInterface.getNetworkInterfaces();
        InetAddress ip = null;
        boolean finded = false;// 是否找到外网IP
        while (netInterfaces.hasMoreElements() && !finded) {
            NetworkInterface ni = netInterfaces.nextElement();
            Enumeration<InetAddress> address = ni.getInetAddresses();
            while (address.hasMoreElements()) {
                ip = address.nextElement();
                if (!ip.isSiteLocalAddress() 
                        && !ip.isLoopbackAddress() 
                        && ip.getHostAddress().indexOf(":") == -1) {// 外网IP
                    netip = ip.getHostAddress();
                    finded = true;
                    break;
                } else if (ip.isSiteLocalAddress() 
                        && !ip.isLoopbackAddress() 
                        && ip.getHostAddress().indexOf(":") == -1) {// 内网IP
                    localip = ip.getHostAddress();
                }
            }
        }
     
        if (netip != null && !"".equals(netip)) {
            return netip;
        } else {
            return localip;
        }
    }
	 
	  public static String getLocalIP() throws Exception{
	  String localIP = "";
	  InetAddress addr = (InetAddress) InetAddress.getLocalHost();
	  //获取本机IP
	  localIP = addr.getHostAddress().toString();
	  return localIP;
	  }
	
	public ServerThread(){
		//创建两个vector数组非常重要,
		//clients负责存储所有与服务器简历的客户端
		//messages负责存储服务器接受到的未发送出去的全部客户单信息
		clients = new Vector<ClientThread>();
		messages = new Vector<Object>();
		bitmaps = new Vector<byte[]>();
		try{
			serverSocket = new ServerSocket(PORT);
		}catch(IOException exception){
			
		}
		try{
			myIPaddress = InetAddress.getLocalHost();
		}catch(UnknownHostException e){
			System.out.println(e.toString());
		}
//		ip = myIPaddress.getHostAddress();
		try {
			ip =  getLocalIP();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sever.jTextArea1.append("服务器地址: "+ip+"端口号："+String.valueOf(serverSocket.getLocalPort())+"\n");
		//创建广播信息线程并启动
		broadcast = new BroadCast(this);
		broadcast.start();
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		super.run();
		while(true){
			try{
				//获取客户端链接，并返回一个新的socket对象
				Socket socket = serverSocket.accept();
				System.out.println(socket.getInetAddress().getHostAddress());
				//创建ClientThread线程并启动
				//创建ClientThread之后，可以监听该连接对应的客户端是否发送来消息，并获取消息
				ClientThread clientThread = new ClientThread(socket,this);
				clientThread.start();
				if(socket!=null){
					synchronized (clients) {
						clients.addElement(clientThread);
					}
				}
			}catch (IOException e){
				System.out.println("发生异常:"+e);
				System.out.println("建议客户端联机失败！");
				System.exit(2);
			}
		}
	}
	
	public void finalize(){
		try{
			serverSocket.close();
		}catch (IOException e){
			serverSocket = null;
		}
	}
	
	
	
}
